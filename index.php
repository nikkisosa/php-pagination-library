<?php include('Custompagination.php');?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="http://localhost/sample/css/custom.css"/>
</head>
<body>
	<?php
	

	$custompagination = new Custompagination(true);

	$total_rows = 100;
	$display_record = 10;

	$uri_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
	$counter = explode('/', $uri_path);
	$item = (!empty($counter[3]) ? $counter[3]:1);

	if(isset($display_record) && !empty($display_record))
	{
		$custompagination->_total_record = $display_record;
		$custompagination->_page_num = $item;
	}
	else
	{
		$custompagination->_page_num = $item;
	}

	$custompagination->_page = 'http://192.168.1.2/procedural/index.php';
	$custompagination->_totalpage = ceil($total_rows/$custompagination->_total_record);
	$custompagination->_paging_class = 'mm-pagination';
	$custompagination->_active_class = 'btn-active';
	$custompagination->_button_class = 'btn-pagination';
	?>
	<div class="pager-bottom">
	<?php
	($custompagination->_totalpage > 0 ? $custompagination->initialize() : '');
	?>
	</div>
</body>
</html>
