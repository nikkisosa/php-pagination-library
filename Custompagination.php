<?php
/**
 * @author Nikki Sosa
 * Date November 09, 2018
 **/
class Custompagination
{
	var $CI;
	/**
	 * @string
	 * page url itself.
	 * ex: $_page = 'https://localhost/sample';
	 **/
	public $_page = ''; 

	/**
	 * set page number.
	 * page number serves as active page.
	 * ex: $_page_num = 1
	 * url output: https://localhost/sample/pro_list?page_num=1
	 **/
	public $_page_num = 0;

	/**
	 * set display record per page.
	 * ex: $_total_record = 10
	 * 
	 **/
	public $_total_record = 10; // default

	/**
	 * set total page in pagination.
	 * ex: formula. 
	 * $total_item = 1000;
	 * $pages = ceil($total_item/$this->custompagination->_total_record);
	 * $_totalpage = $pages;
	 * 
	 **/
	public $_totalpage = 0;

	/**
	 * @string
	 * set page number class
	 **/
	public $_paging_class = 'mm-pagination';

	/**
	 * @string
	 * set page number active class
	 **/
	public $_active_class = 'btn-active';

	/**
	 * @string
	 * set page class affects Previous and Next button
	 **/
	public $_button_class = 'btn-page';

	/**
	 * 
	 **/
	public $_show = ''; // 

	/**
	 * automatic set
	 **/
	private $_max_page = '';
	public $_debug = true;
	public function __construct($isProdecural = false)
	{
		if($isProdecural)
		{

		}
		else
		{
			$this->CI =& get_instance();
	        $this->CI->load->helper('url');
	        $this->CI->config->item('base_url');
		}
	}

	function initialize()
	{
		if($this->_totalpage == 0)
		{
			return FALSE;
		}
		else
		{
			$this->__btn_prev();
			$this->paging();
			$this->__btn_next();
		}
	}

	private function paging()
	{
		try {
			$i = 1;
			for($i;$i<=$this->_totalpage;$i++)
			{

				if($i == $this->_page_num)
				{
					echo '<a href="'.$this->_page.'/'.$i.'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_paging_class.' '.$this->_active_class.'" >'.$i.'</a>';
				}
				else
				{
					echo '<a href="'.$this->_page.'/'.$i.'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_paging_class.'">'.$i.'</a>';
				}
			}
			$this->_max_page = $i-1;
		} catch (Exception $e) {
			show_error($e,405,'Initialize required variable');
		}
	}

	private function __btn_next()
	{
		try {
			if($this->_page_num > 0)
			{
				echo '<a href="'.$this->_page.'/'.($this->_page_num == $this->_max_page ? $this->_page_num : ($this->_page_num+1)).'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_button_class.'">Next</a>';
			}
			else
			{
				echo '<a href="'.$this->_page.'/'.$this->_page_num.'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_button_class.'">Next</a>';
			}
		} catch (Exception $e) {
			show_error($e,405,'Initialize required variable');
		}
	}

	private function __btn_prev()
	{
		try {
			if($this->_page_num > 1)
			{
				echo '<a href="'.$this->_page.'/'.($this->_page_num-1).'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_button_class.'">Previous</a>';
			}
			else
			{
				echo '<a href="'.$this->_page.'/'.$this->_page_num.'?'.$_SERVER['QUERY_STRING'].'" class="'.$this->_button_class.'">Previous</a>';
			}
		} catch (Exception $e) {
			show_error($e,405,'Initialize required variable');
		}
	}

}
