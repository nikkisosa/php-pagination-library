# Custom Pagination
### PHP OOP / Codeigniter3 MVC
![image](/uploads/21afd93afacef90fb3caf89249371b29/image.png)

# How to use
> OOP/Procedural

```php
$custompagination = new Custompagination(true);
```
Note: leave empty if codeigniter.

    Include Custompagination to your file.
![image](/uploads/6634fda5289da0f0181c86dbdf1edad5/image.png)

    Sample Code.
![image](/uploads/4c77d2e2baba05200756b826eb0b0046/image.png)

> Codeigniter

**Add Custompagination to your autoload.php**


In your controller

![image](/uploads/e76d2138a144fa4a9d1db73b95ce2c26/image.png)

In your view

![image](/uploads/596962b6813e3b068fccf71d477af206/image.png)
# Properties
| Variable          |    Type    | Description                    | Value |
| :--------         | :----------| :------------------------------| :-----|
| $_page            |   String   | Set page url.                  | |
| $_page_num        |            | Set page number. page number serves as active page.|
| $_total_record    |            | This serve as total record per page. | 10,50, or 100 |
| $_totalpage       |            | Set total page in pagination. Ex: $total_item = 1000; $pages = ceil($total_item/$this->custompagination->_total_record); $_totalpage = $pages; | |
| $_paging_class    |            | Set page number class          | mm-pagination |
| $_active_class    |            | Set page number active class          | btn-active |
| $_button_class    |            | Set page class affects Previous and Next button          | btn-page |